# LocationSort CLI Tool

<br>
The [LocationSort CLI tool](https://bitbucket.org/Rhdesigns/locationsort/) is a small command line programme written in _Swift_ that takes a remote text file with individual lines of _JSON_ and parses the data. 

Entities within a certain distance of a location are written to _stdout_ 


__Depenedencies__ Swift 4.2

<br>
## Usage

Open terminal and clone the repo to a local folder location
```
git clone git@bitbucket.org:Rhdesigns/locationsort.git
```

Navigate to the new LocationSort directory
```
cd locationsort
```

The locationSort CLI takes 3 arguments as outlined below with their flags.

- __-l__ : Location of the remote file
- __-n__ : Maximum number of customers to print
- __-d__ : The specified distance 

<br>
To run the CLI with the specified requirements copy the code below into the terminal and hit enter

```
./FilterLocation -d 100.00 -n 35 -l https://s3.amazonaws.com/intercom-take-home-test/customers.txt
```

## Sample Output

_Screenshot of CLI output_

![LocationSort output](https://bitbucket.org/Rhdesigns/locationsort/raw/110f2b6c693fe386857db222496eb8e76407b046/LocationSort_SampleOutput.png)