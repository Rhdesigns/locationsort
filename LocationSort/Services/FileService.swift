//
//  FileService.swift
//  LocationSort
//
//  Created by Robert Hunter on 25/01/2019.
//  Copyright © 2019 Robert Hunter. All rights reserved.
//

import Foundation

class FileService {

    func fetchFile(remoteURL: URL, completion: @escaping (_ localURL: URL?) -> Void){
       
        let task = URLSession.shared.downloadTask(with: remoteURL){ localURL, urlResponse, error in
            completion(localURL)
        }
        task.resume()
    }
    
    func parseFile(localURL: URL, completion: @escaping (_ cust: [Customer]?) -> Void) {
        do {
            let customerFile = try String(contentsOf: localURL, encoding: String.Encoding.utf8)
            let data = customerFile.components(separatedBy: .newlines)
            var customers: [Customer] = []
            for cust in data {
                do { // Parses and serialize each valid customer data
                    guard let data = cust.data(using: .utf8) else {
                        continue
                    }
                    let customer = try JSONDecoder().decode(Customer.self, from: data)
                    customers.append(customer)
                }
                catch {
                    continue
                }
            }
             completion(customers)
        }
        catch {
            completion(nil)
        }
    }
}

