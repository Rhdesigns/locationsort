//
//  LocationService.swift
//  LocationSort
//
//  Created by Robert Hunter on 25/01/2019.
//  Copyright © 2019 Robert Hunter. All rights reserved.
//

import Foundation
import CoreLocation

class LocationService {
    
    static func distance(betweenPOIand location:Location) -> Double{
        return Globals.pointOfInterestLocation.distance(from: CLLocation(latitude: location.latitude, longitude: location.longitude))
    }
    
    static func filter(_ customers:[Customer], basedOn criteria: Criteria) -> [Customer]{
        
        // Filter based on specified distance
        var custFiltered = customers.filter{
            let dist = LocationService.distance(betweenPOIand: $0.location) / 1000
            return dist < criteria.distance
        }
        
        // Sort by  UserId
        custFiltered.sort { $0.id < $1.id }
        
        // Return specified number of customers
        return Array(custFiltered.prefix(criteria.numberOfCustomers))
    }
}




