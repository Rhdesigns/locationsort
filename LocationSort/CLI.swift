//
//  CLI.swift
//  LocationSort
//
//  Created by Robert Hunter on 25/01/2019.
//  Copyright © 2019 Robert Hunter. All rights reserved.
//

import Foundation

enum CLIError: Error {
    case InvalidNumberOfArguments
    case InvalidLocationArgument
    case InvalidDistanceArgument
    case InvalidNumberCustomerArgument
    case InvalidCriteriaArguments
}

enum OutputType {
    case stderr
    case stdout
}

class CLI {
    var standardError = FileHandle.standardError
    
    func IO(_ inputs:[String]) -> Criteria?{
        
        var numCust:Int?
        var dist:Double?
        var fileLoc:URL?
        
        do {
            guard Globals.CLI.numberOfArguments == inputs.count else {
                throw CLIError.InvalidNumberOfArguments
            }
            for (index, arg) in inputs.enumerated() {
                switch arg {
                case "-l":
                    // Location argument
                    guard let location = URL(string: inputs[index + 1]) else {
                        throw CLIError.InvalidLocationArgument
                    }
                    fileLoc = location
                    break
                case "-d":
                    // Distance argument
                    guard let distance = Double(inputs[index + 1]) else {
                        throw CLIError.InvalidDistanceArgument
                    }
                    dist = distance
                    break
                case "-n":
                    // Customer Count argument
                    guard let count = Int(inputs[index + 1]) else {
                        throw CLIError.InvalidNumberCustomerArgument
                    }
                    numCust = count
                    break
                default:
                    continue
                }
            }
            
            if let numCust = numCust, let dist = dist, let fileLoc = fileLoc {
                return Criteria(numberOfCustomers:numCust, distance:dist,fileLocation: fileLoc)
            }
            else {
                throw CLIError.InvalidCriteriaArguments
            }
        }
        catch {
            write(msg:"Error parsing CLI arguments. Expected format: \(Globals.CLI.argumentFormat)", dest: .stderr)
            return nil
        }
    
    }
    
    func Execute(criteria:Criteria, completion: @escaping (_ customers:[Customer]?) -> Void){
        // Create file service
        let fileServe = FileService()
        
        // MARK: - 1. Fetch File
        self.write(msg: "Fetching Customers...")
        fileServe.fetchFile(remoteURL: criteria.fileLocation) { (localURL:URL?) in
            guard let local = localURL else {
                self.write(msg: "Error fetching remote file",dest: .stderr)
                return
            }
            
            // MARK: - 2. Parse File
            self.write(msg: "Parsing Customers...")
            fileServe.parseFile(localURL: local, completion: { (custs:[Customer]?) in
                guard let customers = custs else {
                    self.write(msg: "Error parsing customers", dest: .stderr)
                    return
                }
                self.write(msg: "\(customers.count ) Customers found...")
                
                
                // MARK: - 3. Location Service: Return based on criteria
                let filteredCustomers = LocationService.filter(customers, basedOn: criteria)
                self.write(msg: "\(filteredCustomers.count) Customers within \(criteria.distance)km \n")
                completion(filteredCustomers)
            })
        }
    }
    
    private func write(msg: String, dest: OutputType = .stdout ){
        
        switch dest {
        case .stdout:
            print("\u{001B}[0;34m \n\(msg)")
        case .stderr:
            fputs("\u{001B}[0;31m \n\(msg)",stderr)
        }
    }
}
