//
//  Constants.swift
//  LocationSort
//
//  Created by Robert Hunter on 25/01/2019.
//  Copyright © 2019 Robert Hunter. All rights reserved.
//

import Foundation
import CoreLocation

struct Globals {
    static let pointOfInterestLocation = CLLocation(latitude: 53.339428, longitude: -6.257664)
    struct Remote {
        static let fileLocation = "https://s3.amazonaws.com/intercom-take-home-test/customers.txt"
    }
    struct CLI {
        static let argumentFormat = "./executable -l <https://www.file.com/location/> -d <distanceInMeters> -n <maxNumberOfCustomers>"
        static let numberOfArguments = 7
        static let argsDefault = ["-l",Globals.Remote.fileLocation,
                                  "-n","35",
                                  "-d","100.0",
                                  "Debug info"]
    }

}
