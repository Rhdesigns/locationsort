//
//  CLIRunLoop.swift
//  LocationSort
//
//  Created by Robert Hunter on 26/01/2019.
//  Copyright © 2019 Robert Hunter. All rights reserved.
//

import Foundation

// Swift CLI RunLoop
class CLIRunLoop{
    
    private var activeCount = 0
    private let runLoop = RunLoop.current
    
    public init(){}
    
    func starting(){
        activeCount += 1
    }
    
    func finished(){
        activeCount -= 1
    }
    
    func wait(){
        while activeCount > 0 && runLoop.run(mode: RunLoop.Mode.default,
                                             before: Date(timeIntervalSinceNow: 0.1)) {}
    }
}
