//
//  Customer.swift
//  LocationSort
//
//  Created by Robert Hunter on 25/01/2019.
//  Copyright © 2019 Robert Hunter. All rights reserved.
//

import Foundation

struct Customer: Codable {
    var location: Location
    let name: String
    let id: Int
    
    enum CodingKeys: String, CodingKey {
        case name
        case id = "user_id"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.name = try container.decode(String.self, forKey:.name)
        self.id = try container.decode(Int.self, forKey:.id)
        self.location = try Location(from: decoder)
    }
}
