//
//  Location.swift
//  LocationSort
//
//  Created by Robert Hunter on 25/01/2019.
//  Copyright © 2019 Robert Hunter. All rights reserved.
//

import Foundation
import CoreLocation

struct Location: Codable {
    
    let latitude: Double
    let longitude: Double
    
    enum CodingKeys: String, CodingKey {
        case latitude
        case longitude
    }
    
    init(lat:Double, long:Double){
        self.latitude = lat
        self.longitude = long
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        guard let lat = try Double(container.decode(String.self, forKey:.latitude)) else {
            throw DecodingError.typeMismatch(Float.self,
                                             DecodingError.Context(codingPath: container.codingPath,
                                                                   debugDescription: "Casting issue with Latitude"))
        }
        guard let long = try Double(container.decode(String.self, forKey:.longitude)) else {
            throw DecodingError.typeMismatch(Float.self,
                                             DecodingError.Context(codingPath: container.codingPath,
                                                                   debugDescription: "Latitude casting issue"))
        }
        self.latitude = lat
        self.longitude = long
    }
}
