//
//  Criteria.swift
//  LocationSort
//
//  Created by Robert Hunter on 25/01/2019.
//  Copyright © 2019 Robert Hunter. All rights reserved.
//

import Foundation

struct Criteria {
    let numberOfCustomers: Int
    let distance: Double
    let fileLocation: URL
}
