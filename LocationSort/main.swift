//
//  main.swift
//  LocationSort
//
//  Created by Robert Hunter on 25/01/2019.
//  Copyright © 2019 Robert Hunter. All rights reserved.
//
import Foundation


// MARK: - CLI Initialization
let runLoop = CLIRunLoop()
runLoop.starting()
let cli = CLI()

// MARK: - CLI Criteria Setup
guard let userRequest = cli.IO(CommandLine.arguments) else {
    exit(0)
}

// MARK: - CLI Execution of LocationSort
cli.Execute(criteria: userRequest) { (customers:[Customer]?) in
    guard let customers = customers else { exit(0) }
    for cust in customers{
        print("\u{001B}[0;32m Name: \(cust.name) \n  ID: \(cust.id)\n")
    }
    print("\u{001B}[;m")
    runLoop.finished()
}
runLoop.wait()
