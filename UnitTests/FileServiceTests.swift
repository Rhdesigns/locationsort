//
//  FileServiceTests.swift
//  UnitTests
//
//  Created by Robert Hunter on 25/01/2019.
//  Copyright © 2019 Robert Hunter. All rights reserved.
//

import XCTest

class FileServiceTests: XCTestCase {

    override func setUp() {}
    override func tearDown() {}
    
    func testFileServiceFetchFail(){
        // Integration test
        let expectation = self.expectation(description: "FileSavedLocally")
        
        let fileServe = FileService()
        let url = URL(string:"asdfasdfa")!
        fileServe.fetchFile(remoteURL: url) { (localURL:URL?) in
            if(localURL == nil){
                XCTAssertNil(localURL)
                expectation.fulfill()
                return
            }
            XCTFail()
        }
        waitForExpectations(timeout: 30, handler: nil)
    }
    
    func testFileServiceFetchSuccess(){
        // Integration test
        let expectation = self.expectation(description: "FileSavedLocally")
        
        let fileServe = FileService()
        let url = URL(string:Globals.Remote.fileLocation)!
        fileServe.fetchFile(remoteURL: url) { (localURL:URL?) in
            guard let localFile = localURL else {
                XCTFail()
                return
            }
            XCTAssertNotNil(localFile.absoluteString)
            expectation.fulfill()
        }
        waitForExpectations(timeout: 30, handler: nil)
    }
    
    func testFileServiceParseFail(){
        // Integration test
        let expectation = self.expectation(description: "Parser")
        
        let fileServe = FileService()
        let url = URL(string:"https://www.google.ie")!
        fileServe.parseFile(localURL: url, completion: { (customers:[Customer]?) in
            if(customers == nil){
                XCTAssertNil(customers)
                expectation.fulfill()
                return
            }
            XCTFail()
        })
        waitForExpectations(timeout: 30, handler: nil)
    }
    
    func testFileServiceParseSuccess(){
        // Integration test
        let expectation = self.expectation(description: "Parser")
        
        let fileServe = FileService()
        let url = URL(string:Globals.Remote.fileLocation)!
        fileServe.parseFile(localURL: url, completion: { (customers:[Customer]?) in
            guard let custs = customers else {
                XCTFail()
                return
            }
            XCTAssertGreaterThan(custs.count, 0)
            expectation.fulfill()
        })
        waitForExpectations(timeout: 30, handler: nil)
    }
}
