//
//  LocationServiceTests.swift
//  LocationSort
//
//  Created by Robert Hunter on 26/01/2019.
//  Copyright © 2019 Robert Hunter. All rights reserved.
//

import XCTest

class LocationServiceTests: XCTestCase {

    override func setUp() {}

    override func tearDown() {}

    func testLocationServiceWithinDistanceOfPOI(){
        let passingLocations = [Location(lat:53.2451022,long:-6.238335),
                                Location(lat:53.1302756,long:-6.2397222),
                                Location(lat:53.1229599,long:-6.2705202)]
        
        for passLocale in passingLocations {
            let distanceFromPOI = LocationService.distance(betweenPOIand: passLocale) / 1000
            XCTAssertLessThan(distanceFromPOI, 100)
        }
    }
    
    func testLocationServiceOutsideDistanceOfPOI(){
        let failingLocations = [Location(lat:53.4692815,long:-9.436036),
                                Location(lat:52.2559432,long:-7.1048927),
                                Location(lat:52.240382,long:-6.972413)]
        
        for failLocale in failingLocations {
            let distanceFromPOI = LocationService.distance(betweenPOIand: failLocale) / 1000
            XCTAssertGreaterThan(distanceFromPOI, 100)
        }
    }
}
