//
//  CLITests.swift
//  LocationSort
//
//  Created by Robert Hunter on 25/01/2019.
//  Copyright © 2019 Robert Hunter. All rights reserved.
//

import XCTest

class CLITests: XCTestCase {
    
    static let argsFail = ["-l","invalid",
                           "-n","ShouldBeAnInt",
                           "-d","Should be a float or double"]
    
    override func setUp() {
    }

    override func tearDown() {
    }
    
    func testCLICriteriaCreationFail() {
        let cli = CLI()
        let criteria = cli.IO(CLITests.argsFail)
        XCTAssertNil(criteria)
    }

    func testCLICriteriaCreationSuccess() {
        let cli = CLI()
        let crit = cli.IO(Globals.CLI.argsDefault)
        guard let criteria = crit else {
            XCTFail()
            return
        }
        XCTAssert(criteria.distance == 100.00)
        XCTAssert(criteria.numberOfCustomers == 35)
        XCTAssert(criteria.fileLocation == URL(string:"https://s3.amazonaws.com/intercom-take-home-test/customers.txt")!)
    }
    
    func testCLITooFewArguments(){
        let cli = CLI()
        let criteria = cli.IO(["Too","Few","Arguments"])
        XCTAssertNil(criteria)
    }
    
    func testCLIIntegration(){
        let expectation = self.expectation(description: "FileSavedLocally")
        
        let cli = CLI()
        guard let userReq = cli.IO(Globals.CLI.argsDefault) else {
            XCTFail()
            return
        }
        
        cli.Execute(criteria: userReq){ (custs:[Customer]?) in
            guard let customers = custs else {
                XCTFail()
                return
            }
            XCTAssertEqual(16, customers.count)
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 30, handler: nil)
    }
}
